# Apps

Репозиторий хранит конфигурацию инфраструктуры для разворачивания k8s кластера из 2 нод - мастера и воркера. А так же, создания и настройки дополнительного инстанса для мониторинга, сборки образов и тп.

Для разворачивания инфраструктуры потребуется: 
* terraform (версии больше 0.13)
* ansible (версия болше 2.10.11)

# План
**1. ИНФРАСТРУКТУРА**

**1.1**  С помощью terraform создать инстансы в Ynadex Cloud.

  Для авторизации необходимо добавить даныне в переменные окружения
  
```
  export YC_TOKEN=$(yc iam create-token)
  export YC_CLOUD_ID=$(yc config get cloud-id)
  export YC_FOLDER_ID=$(yc config get folder-id)
```
  В главном файле конфигурации main.tf изменить путь до ssh ключа в переменной ansible_ssh, который будет передаваться на новые инстансы для возможно авторизации. 

```
  cd apps/terraform/
  terraform apply
```

**1.2.1** Конфигурация клстера K8S
 
  Ansible находиться на той же ВМ что и Terraform
   
  Для разворачивания k8s кластера необходимо запустить команду
  
  `ansible-playbook kubespray/cluster.yml -b -i kubespray/inventory.ini`
  
**1.2.2** Конфигурация инстансов

  Подготовте конфигурацию для своих целей - измените именя сервера zabbix (строка 14), URL адрес ci/cd сервера (строка 45), имя сервера docker registry (строка 137).
  
  ```
  echo "gitlab_token: <USREGITLABRUNNERTOKENT>" > gitlab-token.yml # создайте файл с токеном GitLab runner в качестве переменной
  ansible-vault encrypt gitlab-token.yml # шифрование файла Паролем
  ansible-playbook config.yml --ask-vault-pass # запуск playbook с запросом Пароля для расшифровки
  ```

- установка helm на master ноду
- установка docker, docker-compose, gitlab-runner, zabbix-agent
- конфигурация прав доступа 
- запуск и настройка локального docker regestry
- запуск и регистрация gitlab-runner

**2. Развертование проекта на кластере K8S через CI/CD GitLab**

  `.gitlab-ci.yml`

  Соберет склонированный проект и отправит в локальное docker regestry. А так же развернет приложение в кластере Kuber
 