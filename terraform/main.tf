terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-b"
}

#===================================================================
 resource "yandex_compute_instance" "srv" {
   name     = "srv"
   hostname = "srv"

   resources {
     cores  = 2
     memory = 2
   }

   boot_disk {
    initialize_params {
      image_id = "fd864gbboths76r8gm5f" # 2204
      size = 50
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "master" {
  name     = "master"
  hostname = "master"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8gnpl76tcrdv0qsfko" # 2004
      size = 50
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "worker" {
  name     = "worker"
  hostname = "worker"

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd8gnpl76tcrdv0qsfko" # ubuntu 2004 lts (yc compute image list --folder-id standard-images | grep ubuntu-2204)
      size = 50
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

# ==============================================

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["172.20.0.0/24"]
}

# ==============================================

output "srv-ansible_host" {
  value = yandex_compute_instance.srv.network_interface.0.ip_address
}

output "master-ansible_host" {
  value = yandex_compute_instance.master.network_interface.0.ip_address
}

output "worker-ansible_host" {
  value = yandex_compute_instance.worker.network_interface.0.ip_address
}
